import { prisma } from '$lib/server/prisma.js';

export const GET = async ({ request, url }) => {
	/*
    const authHeader = request.headers.get('Authorization')

    if (authHeader !== AUTH_TOKEN) {
        return new Response(JSON.stringify({message: "Invalid credentials"}), {status: 401})
    }
    */

	const id = url.searchParams.get('id');

	const headers = {
		'Content-Type': 'application/json'
	};

	if (!id) {
		return new Response(JSON.stringify({ error: 'Please provide an id' }), {
			status: 400,
			headers
		});
	}

	try {
		const results = await prisma.items.findMany({
			where: {
				uiid: Number(id)
			}
		});

		return new Response(JSON.stringify({ data: results }), { status: 200, headers });
	} catch (error) {
		return new Response(JSON.stringify({ error: `Internal Server Error  ${error}` }), {
			status: 500,
			headers
		});
	}
};
