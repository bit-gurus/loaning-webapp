import { prisma } from '$lib/server/prisma.js';

export const GET = async ({ request, url }) => {
	/*
    const authHeader = request.headers.get('Authorization')

    if (authHeader !== AUTH_TOKEN) {
        return new Response(JSON.stringify({message: "Invalid credentials"}), {status: 401})
    }
    */

	const headers = {
		'Content-Type': 'application/json'
	};

	try {
		const results = await prisma.students_x_items.findMany({
			where: {
				time_returned: null
			},
			include: {
				students: true,
				items: true
			}
		});

		return new Response(JSON.stringify({ data: results }), { status: 200, headers });
	} catch (error) {
		return new Response(JSON.stringify({ error: 'Internal Server Error' }), {
			status: 500,
			headers
		});
	}
};
