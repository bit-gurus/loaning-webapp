import { prisma } from '$lib/server/prisma';
import type { PageServerLoad } from './$types';

export const load: PageServerLoad = async () => {
	return {
		loaned_items: await prisma.students_x_items.findMany({
			where: {
				time_returned: null
			},
			include: {
				students: true,
				items: true
			}
		}),
		items: await prisma.items.findMany()
	};
};
