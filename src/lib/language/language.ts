import { writable } from 'svelte/store';
import translations from './translations.json';

export const selectedLanguage = writable('de'); // Default language is English

export function getTranslation(key) {
	const currentLanguage = selectedLanguage; // Access the store
	return translations[currentLanguage].key;
}
