import { writable } from 'svelte/store';

export const latestSerialData = writable('');
let port;

export const connectToArduino = async () => {
	try {
		port = await navigator.serial.requestPort();
		await port.open({ baudRate: 9600 });
		const reader = port.readable.getReader();

		let partialLine = ''; // To handle partial lines
		while (true) {
			const { value, done } = await reader.read();
			if (done) break;

			const text = new TextDecoder().decode(value);
			partialLine += text;

			// Check if a full line has been received (ends with a newline character)
			if (partialLine.includes('\n')) {
				const lines = partialLine.split('\n');
				latestSerialData.set(lines[lines.length - 2]); // Use the second-to-last line as the latest
				partialLine = lines[lines.length - 1]; // Store the remaining partial line
			}
		}
	} catch (error) {
		console.error(error);
	}
};
