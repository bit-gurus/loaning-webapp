-- CreateTable
CREATE TABLE `cards` (
    `ucid` VARCHAR(12) NOT NULL,
    `is_locked` TINYINT NULL,
    `is_lost` TINYINT NULL,

    UNIQUE INDEX `UCID_UNIQUE`(`ucid`),
    PRIMARY KEY (`ucid`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `items` (
    `uiid` INTEGER NOT NULL,
    `name` VARCHAR(45) NULL,
    `amount_original` INTEGER NOT NULL,
    `amount_availabe` INTEGER NOT NULL,
    `note` VARCHAR(300) NULL,
    `price` DOUBLE NULL,

    UNIQUE INDEX `UIID_UNIQUE`(`uiid`),
    PRIMARY KEY (`uiid`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `students` (
    `uuid` INTEGER NOT NULL,
    `firstname` VARCHAR(45) NULL,
    `lastname` VARCHAR(45) NULL,

    UNIQUE INDEX `UUID_UNIQUE`(`uuid`),
    PRIMARY KEY (`uuid`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `students_x_cards` (
    `uchid` INTEGER NOT NULL,
    `students_uuid` INTEGER NOT NULL,
    `cards_ucid` VARCHAR(12) NOT NULL,
    `time_recieved` DATETIME(0) NULL,
    `time_retracted` DATETIME(0) NULL,

    INDEX `fk_students_has_cards1_cards1_idx`(`cards_ucid`),
    INDEX `fk_students_has_cards1_students1_idx`(`students_uuid`),
    PRIMARY KEY (`uchid`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `students_x_items` (
    `uihid` INTEGER NOT NULL,
    `students_uuid` INTEGER NOT NULL,
    `items_uiid` INTEGER NOT NULL,
    `time_loaned` DATETIME(0) NULL,
    `time_returned` DATETIME(0) NULL,

    UNIQUE INDEX `UIHID`(`uihid`),
    INDEX `fk_students_has_items1_items1_idx`(`items_uiid`),
    INDEX `fk_students_has_items1_students1_idx`(`students_uuid`),
    PRIMARY KEY (`uihid`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `students_x_cards` ADD CONSTRAINT `fk_students_has_cards1_cards1` FOREIGN KEY (`cards_ucid`) REFERENCES `cards`(`ucid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE `students_x_cards` ADD CONSTRAINT `fk_students_has_cards1_students1` FOREIGN KEY (`students_uuid`) REFERENCES `students`(`uuid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE `students_x_items` ADD CONSTRAINT `fk_students_has_items1_items1` FOREIGN KEY (`items_uiid`) REFERENCES `items`(`uiid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

-- AddForeignKey
ALTER TABLE `students_x_items` ADD CONSTRAINT `fk_students_has_items1_students1` FOREIGN KEY (`students_uuid`) REFERENCES `students`(`uuid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

