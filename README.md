# loaning-webapp

A _simple_ webapp written in Svelte for schools to loan out items to students via a RFID card system.

## Notice

This app is still in very early developement. Features may be broken and it's still only a demo for now.

## Running the webapp

If you want to run the webapp in it's current state you need to edit the `.env.example file` to your liking and rename it to `.env`.

Then you can start the webapp like this:

```bash
npm run dev
```
